set(PLUG_DIR ${PLUGIN_PRIVATE_MODULE_DIR}/Lomiri/SystemSettings/SecurityPrivacy)

set(CMAKE_AUTOMOC ON)

set(HELPER_SOURCES
    polkitlistener.cpp
    helper.cpp
)
add_executable(LomiriSecurityPrivacyHelper ${HELPER_SOURCES})

target_include_directories(LomiriSecurityPrivacyHelper PRIVATE
    ${POLKIT_AGENT_INCLUDE_DIRS}
    ${LIBSYSTEMD_INCLUDE_DIRS})
target_compile_definitions(LomiriSecurityPrivacyHelper PRIVATE
    POLKIT_AGENT_I_KNOW_API_IS_SUBJECT_TO_CHANGE)
target_link_libraries(LomiriSecurityPrivacyHelper
    ${POLKIT_AGENT_LDFLAGS}
    ${LIBSYSTEMD_LDFLAGS})

set(QML_SOURCES
    AppAccess.qml
    AppAccessControl.qml
    here-terms.qml
    Location.qml
    LockSecurity.qml
    Ofono.qml
    PageComponent.qml
    PhoneLocking.qml
    PincodePrompts.qml
    SimPin.qml
    sims.js
)
set(PANEL_SOURCES
    connectivity.cpp
    connectivity.h
    plugin.cpp
    plugin.h
    securityprivacy.cpp
    securityprivacy.h
    trust-store-model.cpp
    trust-store-model.h
)
add_library(LomiriSecurityPrivacyPanel MODULE ${PANEL_SOURCES} ${QML_SOURCES})

target_compile_definitions(LomiriSecurityPrivacyPanel PRIVATE
    HELPER_EXEC="${PLUG_DIR}/LomiriSecurityPrivacyHelper"
    QT_NO_KEYWORDS)
target_include_directories(LomiriSecurityPrivacyPanel PRIVATE
    ${Intl_INCLUDE_DIRS}
    ${TRUST_STORE_INCLUDE_DIRS}
    ${GLIB_INCLUDE_DIRS}
)
target_link_libraries (LomiriSecurityPrivacyPanel
    ${Intl_LIBRARIES}
    Qt5::Core
    Qt5::Gui
    Qt5::Qml
    Qt5::Quick
    Qt5::DBus
    LomiriSystemSettingsPrivate
    ${TRUST_STORE_LDFLAGS}
    ${GLIB_LDFLAGS}
)

install(TARGETS LomiriSecurityPrivacyPanel LomiriSecurityPrivacyHelper
    DESTINATION ${PLUG_DIR})
install(FILES qmldir DESTINATION ${PLUG_DIR})
install(FILES ${QML_SOURCES} DESTINATION ${PLUGIN_QML_DIR}/security-privacy)
install(FILES settings-security-privacy.svg
    DESTINATION ${PLUGIN_MANIFEST_DIR}/icons)
install(FILES security-privacy.settings DESTINATION ${PLUGIN_MANIFEST_DIR})
