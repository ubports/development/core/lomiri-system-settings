import QtQuick 2.12
import SystemSettings 1.0
import SystemSettings.ListItems 1.0 as SettingsListItems
import Lomiri.Components.ListItems 1.3 as ListItem
import Lomiri.Components 1.3
import Lomiri.SystemSettings.Usb 1.0

ItemPage {
    id: root
    title: i18n.tr("USB")
    flickable: usbFlickable

    Flickable {
        id: usbFlickable
        anchors.fill: parent
        contentHeight: configLayout.height + configLayout.anchors.bottomMargin + configLayout.anchors.topMargin
        boundsBehavior: (contentHeight > root.height) ?
                            Flickable.DragAndOvershootBounds :
                            Flickable.StopAtBounds
        /* Workaround https://bugreports.qt-project.org/browse/QTBUG-31905 */
        flickableDirection: Flickable.VerticalFlick

        Column {
            id: configLayout
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.topMargin: units.gu(1)

            SettingsItemTitle {
                text: i18n.tr("Use USB for:")
            }
            ListItem.ItemSelector {
                expanded: true
                model: [i18n.tr("Charging only"),
                        i18n.tr("File transfer"),
                        i18n.tr("USB tethering")]
                selectedIndex: UsbConfig.mode
                onSelectedIndexChanged: UsbConfig.mode = selectedIndex
            }

            ListItem.Divider {}
        }
    }
}
